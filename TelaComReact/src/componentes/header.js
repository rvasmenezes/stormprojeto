import React, { Component } from 'react';

class Header extends Component {

    render() {

        return (

            <div className="divHeader row">
                <div className="col-sm-3 divIconeEsquerda" align="left">
                    <img src={require("./../image/ico_header_palitos.png")} />
                    <img src={require("./../image/ico_barra_separacao.png")} />
                    <img src={require("./../image/ico_header_escudo_pessoa.png")} />
                </div>
                <div className="col-sm-3" align="left">
                    <input className="form-control" type="text" placeholder="Pesquisar..." aria-label="Search" className="campoPesquisa" />
                    <i className="fas fa-search" aria-hidden="true" />
                </div>
                <div className="col-sm-6 divIcone" align="right">
                    <img src={require("./../image/ico_opcoes.png")} style={{ marginRight: -20 }} data-toggle="modal" data-target="#myModal2" />
                    <img src={require("./../image/btnIncluirUsuario.png")} />
                    <img src={require("./../image/ico_barra_separacao.png")} />
                    <img src={require("./../image/ico_header_casa.png")} />
                    <img src={require("./../image/ico_header_configuracoes.png")} />
                    <img src={require("./../image/ico_logout.png")} />
                </div>
            </div>

        );
    }

}
export default Header;
