import React, { Component } from 'react';

export default class Conversor extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: ""
        };

        this.converter = this.converter.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //console.log(this.props.moedaA_valor)
    }

    handleChange(event) {
        this.setState({value: event.target.value});
        //alert(this.state.value);
        //console.log(this.state.value)
    }

    handleSubmit(event) {
        alert('Um nome foi enviado: ' + this.state.value);
        event.preventDefault();

        this.converter();
    }

    converter() {
        
        //alert(this.props.moedaA_valor);
        //console.log(this.props.moedaA_valor)
        let url = `https://jsonplaceholder.typicode.com/posts/${this.state.value}`

        fetch(url)
            .then(res => res.json())
            .then(json => {
                console.log(json.userId);
                //let cotacao = json[de_para].val;
                //let moedaB_valor = this.state.moedaA_valor * cotacao
            })

    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>
                <label>
                Nome:
                <input type="text" value={this.state.value} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Enviar" />
            </form>

        );
    }
}