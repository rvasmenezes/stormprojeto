import React from 'react';

export default class Table extends React.Component {
    
    constructor(props){
      super(props);
      this.getHeader = this.getHeader.bind(this);
      this.getRowsData = this.getRowsData.bind(this);
      this.getKeys = this.getKeys.bind(this);
    }
    
    getKeys = function(){
      return Object.keys(this.props.data[0]);
    }
    
    getHeader = function(){
      var keys = this.getKeys();
      return keys.map((key, index)=>{
        return <th key={key}>{key.toUpperCase()}</th>
      })
    }
    
    getRowsData = function(){
      var items = this.props.data;
      var keys = this.getKeys();
      return items.map((row, index)=>{
        return <tr key={index}> 
                    <td className="tdCheck"> 
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck" name="example1" />
                            <label class="custom-control-label" for="customCheck"></label>
                            </div>                                            
                    </td>
                    <RenderRow key={index} data={row} keys={keys}/>
                </tr>
      })
    }
    
    render() {
        return (
            <tbody>
                {this.getRowsData()}                                                               
            </tbody>
          
        );
    }
}

const RenderRow = (props) =>{
  return props.keys.map((key, index)=>{    

    //console.log(props.data[index]);
        if(props.data[key] == "ATIVO"){
            return <td key={props.data[key]} className="tdStatus" >{props.data[key]}</td>
        }else{
            return <td key={props.data[key]}  >{props.data[key]}</td>
        }
  })
}