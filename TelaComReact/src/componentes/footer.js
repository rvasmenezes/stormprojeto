import React, { Component } from 'react';

class Footer extends Component {

    render() {

        return (

            <div class="divFooter">
                <div class="row" style={{margin: 0}}>
                    <div class="col-md-1 noAr" align="left">
                        NO AR
                            </div>
                    <div class="col-md-2 text-white" align="left">
                        <span>ENCONTRO - 10:00</span>
                    </div>
                    <div class="col-md-2 text-white" align="left">
                        <span><i>Última atualização em 10:28</i></span>
                    </div>
                    <div class="col-md-5 text-white" align="right">
                        <span><i>Quinta, 30 de Maio de 2019</i></span>
                    </div>
                    <div class="col-md-2" align="right" style={{color: '#FF8700', fontSize: 25, fontFamily: 'fantasy', marginTop: -7}}>
                    // 10:30:42
                    </div>
                </div>
            </div>

        );
    }

}
export default Footer;
