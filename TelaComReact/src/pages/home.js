import React, { Component } from 'react';
import Header from '../componentes/header';
import Footer from '../componentes/footer';
import Table from '../componentes/Table';

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: [
                { 'Nome': 'Maria', 'Email': 'maria@tvglobo.com.br', 'DtInclusao': '10/01/2020', 'DtAlteracao': '11/01/2020', 'Regras': '01', 'Status': 'ATIVO', 'Ações': '...' },
                { 'Nome': 'Maria', 'Email': 'maria@tvglobo.com.br', 'DtInclusao': '10/01/2020', 'DtAlteracao': '11/01/2020', 'Regras': '01', 'Status': 'ATIVO', 'Ações': '...' },
                { 'Nome': 'Maria', 'Email': 'maria@tvglobo.com.br', 'DtInclusao': '10/01/2020', 'DtAlteracao': '11/01/2020', 'Regras': '01', 'Status': 'ATIVO', 'Ações': '...' },
                { 'Nome': 'Maria', 'Email': 'maria@tvglobo.com.br', 'DtInclusao': '10/01/2020', 'DtAlteracao': '11/01/2020', 'Regras': '01', 'Status': 'ATIVO', 'Ações': '...' },
                { 'Nome': 'Maria', 'Email': 'maria@tvglobo.com.br', 'DtInclusao': '10/01/2020', 'DtAlteracao': '11/01/2020', 'Regras': '01', 'Status': 'ATIVO', 'Ações': '...' },
                { 'Nome': 'Maria', 'Email': 'maria@tvglobo.com.br', 'DtInclusao': '10/01/2020', 'DtAlteracao': '11/01/2020', 'Regras': '01', 'Status': 'ATIVO', 'Ações': '...' },
                { 'Nome': 'Maria', 'Email': 'maria@tvglobo.com.br', 'DtInclusao': '10/01/2020', 'DtAlteracao': '11/01/2020', 'Regras': '01', 'Status': 'ATIVO', 'Ações': '...' },
                { 'Nome': 'Maria', 'Email': 'maria@tvglobo.com.br', 'DtInclusao': '10/01/2020', 'DtAlteracao': '11/01/2020', 'Regras': '01', 'Status': 'ATIVO', 'Ações': '...' },            
            ]
        }
    }

    render() {

        return (

            <div>
                <Header />

                <div className="divTable">
                    <form class="form-inline">
                        <table id="mytable" className="table table-bordred table-striped">

                            <thead style={{ backgroundColor: '#FFFFFF', fontSize: 11.5 }}>
                                <th></th>
                                <th>USUÁRIO</th>
                                <th>EMAIL</th>
                                <th>DATA DE INCLUSÃO</th>
                                <th>DATA DE ALTERAÇÃO</th>
                                <th>REGRAS</th>
                                <th>STATUS</th>
                                <th>AÇÕES</th>
                            </thead>
                            <Table data={this.state.tableData}/>
                        </table>
                    </form>

                    <div style={{ paddingTop: 20 }}>
                        <a href="#" className="btnSecundario">Primeiro</a>
                        <a href="#" className="btnSecundario">Anterior</a>
                        <a href="#" className="btnPrincipal" onClick={this.converter}>1</a>
                        <a href="#" className="btnSecundario">Próximo</a>
                        <a href="#" className="btnSecundario">Último</a>
                    </div>
                </div>

                <Footer />
            </div>
        );

    }

}
export default Home;