﻿using System;

namespace ProjetoDesafio
{
    public class Questao1
    {
        public static void RetornaIndices()
        {
            int[] array = new int[6];
            Console.WriteLine("Digite 6 números para compor o array:");
            for (int i = 0; i < 6; i++)
            {

                string Input = Console.ReadLine();
                if (int.TryParse(Input, out int Number))
                {
                    array[i] = Number;
                }
                else
                {
                    Console.WriteLine("Digite um número inteiro!");
                    i--;
                }
            }

            Console.WriteLine("===============================================");
            Console.WriteLine("{0}{1,8}", "Índice", "Valor");
            for (int counter = 0; counter < 6; counter++)
                Console.WriteLine("{0,5}{1,8}", counter, array[counter]);

            Console.WriteLine("\r\n");
            Console.Write("Digite o número para encontrar os indices: ");
            int nBusca = Convert.ToInt32(Console.ReadLine());

            string indices = "", valores = "";
            for (int counter = 0; counter < 6; counter++)
            {
                int nProximo = counter + 1;
                for (int counter2 = nProximo; counter2 < 6; counter2++)
                {
                    if (array[counter] + array[counter2] == nBusca)
                    {
                        indices = "Índices: [" + counter.ToString() + ", " + counter2.ToString() + "]";
                        valores = "Valores: " + array[counter].ToString() + " + " + array[counter2].ToString() + " = " + nBusca;
                        break;
                    }

                }
            }

            if (indices == "")
            {
                indices = "Nenhum índice encontrado!";
                valores = "Nenhum valor encontrado!";
            }
                
            Console.WriteLine(indices);
            Console.WriteLine(valores);
            Console.Write("\r\nPrecione ENTER e volta ao menu...");
            Console.ReadLine();
        }
    }
}
