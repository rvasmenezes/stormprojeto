﻿using System;

namespace ProjetoDesafio
{
    public class Questao4
    {
        public static void RetornaRetencao()
        {
            Console.WriteLine("[MAPA DE ELEVAÇÃO] DIGITE OS VALORES SEPARADOS POR VÍRGULA. EX.: 2,4,1,5,6 ");
            string txtValores = Console.ReadLine().ToString();

            var arrayVal = txtValores.Split(",");
            int somaBacia = 0, vlMenor = 0, vlMaior = 0, vl = Convert.ToInt32(arrayVal[0]);

            for (int i = 1; i < arrayVal.Length; i++)
            {

                bool blocoExistente = ExisteBlocoMaiorOuIgualNaFrente(i, vl, arrayVal);
                if (!blocoExistente)
                {
                    //Pega o próximo maior
                    for (int frente = i; frente < arrayVal.Length; frente++)
                    {
                        if (vlMaior < Convert.ToInt32(arrayVal[frente]))
                            vlMaior = Convert.ToInt32(arrayVal[frente]);
                    }

                    vl = vlMaior;

                    blocoExistente = ExisteBlocoMaiorOuIgualNaFrente(i, vl, arrayVal);
                }

                if(vl > Convert.ToInt32(arrayVal[i]) && blocoExistente)
                {
                    vlMenor = Convert.ToInt32(arrayVal[i]);
                    somaBacia = somaBacia + vl - vlMenor;
                }
                else
                {
                    vl = Convert.ToInt32(arrayVal[i]);
                }
            }

            Console.WriteLine("\nTotal de água retida: " + somaBacia);
            Console.Write("\r\nPrecione ENTER e volta ao menu...");
            Console.ReadLine();
        }

        private static bool ExisteBlocoMaiorOuIgualNaFrente(int i, int vl, string[] array)
        {
            for (int frente = i; frente < array.Length; frente++)
            {
                if (vl <= Convert.ToInt32(array[frente]))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
