﻿using System;

namespace ProjetoDesafio
{
    class Program
    {
        static void Main(string[] args)
        {

            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }

        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Escolha a questão para teste e clique ENTER:");
            Console.WriteLine("1) Retorna índices.");
            Console.WriteLine("2) Verifica se brackets são balanceados.");
            Console.WriteLine("3) Verifica lucro máximo.");
            Console.WriteLine("4) Retenção de água após chuva.");
            Console.Write("\r\n");

            switch (Console.ReadLine())
            {
                case "1":
                    Questao1.RetornaIndices();
                    return true;
                case "2":
                    Questao2.ValidaBracketes();
                    return true;
                case "3":
                    Questao3.RetornaLucroMaximo();
                    return false;
                case "4":
                    Questao4.RetornaRetencao();
                    return false;
                default:
                    return true;
            }
        }

    }
}
