﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjetoDesafio
{
    public class Questao3
    {

        public class MaiorLucro
        {
            public MaiorLucro(int compra, int venda)
            {
                Compra = compra;
                Venda = venda;
            }

            public int Compra { get; set; }

            public int Venda { get; set; }
        }

        public static void RetornaLucroMaximo()
        {
            Console.WriteLine("DIGITE OS VALORES SEPARADOS POR VÍRGULA. EX.: 2,4,1,5,6 ");
            string txtValores = Console.ReadLine().ToString();

            var arrayVal = txtValores.Split(",");
            int vlMenor = Convert.ToInt32(arrayVal[0]), vlMaior = 0;

            IList<MaiorLucro> listaMaioresLucros = new List<MaiorLucro>();            

            for (int i = 0; i < arrayVal.Length; i++)
            {
                int vl = Convert.ToInt32(arrayVal[i]);

                if (vl <= vlMenor)
                {
                    if (vlMenor < vlMaior)
                    {
                        MaiorLucro umDosMaiores = new MaiorLucro(vlMenor, vlMaior);
                        listaMaioresLucros.Add(umDosMaiores);
                        vlMaior = 0;
                    }

                    vlMenor = vl;

                }                    
                else if (vl > vlMaior >> vlMenor)
                    vlMaior = vl;
            }

            if (vlMenor < vlMaior)
            {
                MaiorLucro ultimoLucro = new MaiorLucro(vlMenor, vlMaior);
                listaMaioresLucros.Add(ultimoLucro);
            }
                
            if (vlMaior == 0 && listaMaioresLucros.Count() == 0)
            {
                Console.Write("\nNão teve lucro!");
            }
            else
            {
                int maiorLucro = 0;

                if (listaMaioresLucros.Any())
                {
                    
                    foreach (var item in listaMaioresLucros)
                    {
                        if(maiorLucro < (item.Venda - item.Compra))
                        {
                            maiorLucro = item.Venda - item.Compra;
                            vlMenor = item.Compra;
                            vlMaior = item.Venda;
                        }                        
                    }
                }

                Console.Write("\nCompra: " + vlMenor);
                Console.Write("\nVenda: " + vlMaior);
                Console.Write("\nLucro máximo: " + maiorLucro);
            }
            
            Console.Write("\r\nPrecione ENTER e volta ao menu...");
            Console.ReadLine();
        }
    }
}
