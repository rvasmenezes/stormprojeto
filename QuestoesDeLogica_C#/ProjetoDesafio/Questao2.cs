﻿using System;

namespace ProjetoDesafio
{
    public class Questao2
    {
        public static void ValidaBracketes()
        {
            Console.Write("DIGITE OS BRACKETES: ");
            string txtBracketes = Console.ReadLine().ToString();

            bool retorno = true;

            if (txtBracketes.Length % 2 != 0)
                retorno = false;

            if(retorno)
                for (int i = 0; i < txtBracketes.Length/2; i++)
                {

                    if (txtBracketes[i].ToString() == "{" && txtBracketes[txtBracketes.Length - 1 - i].ToString() != "}")
                        retorno = false;
                    else if (txtBracketes[i].ToString() == "[" && txtBracketes[txtBracketes.Length - 1 - i].ToString() != "]")
                        retorno = false;
                    else if (txtBracketes[i].ToString() == "(" && txtBracketes[txtBracketes.Length - 1 - i].ToString() != ")")
                        retorno = false;

                    if (!retorno)
                        break;

                }

            Console.Write("Bracketes balanceados: ");
            if (retorno)
                Console.WriteLine("SIM");
            else
                Console.WriteLine("NÂO");

            Console.Write("\r\nPrecione ENTER e volta ao menu...");
            Console.ReadLine();
        }
    }
}
